---
layout: markdown_page
title: "GitLab Release Posts"
description: "Guidelines to create and update release posts"
---

<br>

### Release Blog Posts Handbook
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Release posts

GitLab releases a new version every 22nd of each month,
and announces it through monthly release posts.

Patch and security issues are addressed more often,
whenever necessary.

- For a list of release posts, please check the
category [release](/blog/categories/release/).
- For a list of security releases, please check
the category [security release](/blog/categories/security-release/).
- For a list of features per release, please check
the [release list](/release-list/).
- For all named changes, please check the changelog
for [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md)
and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CHANGELOG.md).
- See also [release managers](/release-managers/).

### Templates

To start a new release post, please choose one of
these templates, and follow their instructions
to insert content. Please make sure to use
the most recent template available.

- [Monthly release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/release_blog_template.html.md)
- [Patch release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/patch_release_blog_template.html.md)
- [Security release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/security_release_blog_template.html.md)

For patch and security releases, please make sure
to specify them in the title, add the correct [category](../#categories):

- Patch releases: 
  - `title: "GitLab Patch Release: x.x.x and x.x.x"`
  - `categories: release`
- Security releases:
  - `title: "GitLab Security Release: x.x.x and x.x.x"`
  - `categories: security release`

----

## Monthly releases

Monthly releases have an exclusive layout aiming to appraise the reader
with the presentation of a new release every 2nd.

**Note:** The new design for monthly release posts was
 [introduced](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4780)
in March 2017 with the release of
[GitLab 9.0](/2017/03/22/gitlab-9-0-released/).
The new layout was [introduced](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/5937) in May 2017 with the release of [GitLab 9.2](/2017/03/22/gitlab-9-2-released/).

### Getting started

To create a new monthly release post, add two files to the [about.GitLab.com repository]() (consider the release of GitLab X.Y, released in AAAA/MM/DD):

- A YAML data file, containing all the release post content
  - Into `data/release_posts/`, add a new file called `AAAA_MM_22_gitlab_X_Y_released.yml`
  - Template ([latest version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/AAAA_MM_DD_gitlab_x_y_released.yml))
- A blog post file, containing the introduction and the blog post frontmatter information
  - Into `source/posts/`, add a new file called `AAAA-MM-22-gitlab-X-Y-released.html.md`
  - Template ([latest version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/release_blog_template.html.md))

**Important!** Please use the most recent templates for each of these files.

#### Merge Request

Create a merge request with the introductory changes _before the kick off call_
to make the post available to receive contributions from the team.

![release post MR template](release-post-mr-template.png){:.shadow}

<!-- #### Authorship

Each month a Product Manager will lead the release post, being accountable for:

- Creating the merge request
- Writing the blog post introduction
- Adding the [MVP](#mvp)
- Adding the [cover image](#cover-image) and the [social sharing image](#social-sharing-image)
- Updating the promo file (`source/data/promo.yml`)
- Making sure all feature descriptions are positive and cheerful
- @mentioning `@all` in the MR thread to remind everyone about the post and the due dates
- Helping to solve all comments in the thread
- Making sure all images (png, jpg, and gifs) are smaller than 300 KB each
- Merging the post on the 22nd
- Posting on Social Media (Twitter/Facebook) -->

#### Stages of contribution

Monthly release posts are created in two stages:

- **General contributions**: everyone can contribute!
In this stage, team members will add features and their
respective images, videos, etc.
- **Review**: 
  - **Content**: PMs check and review the content,
writers/editors copyedit anything necessary
  - **Structure**: tech writing/frontend/ux check the syntax
  and the structure of the blog post.

To be able to finish our work in time, with no rush,
each stage will have its due date.

### Due dates

To having the release post well written and ready in
time for the release date, please set due dates for:

- [General contributions](#general-contributions)
from the team: 6th working day before the 22nd
- [Review](#review): 4th working day before the 22nd

Ideally, the 3rd and the 2nd working day before the release
should be left for fixes and small improvements.

### General contributions

Added by the team until the 6th working day before
the 22nd. Please fill all the [sections](#sections).

#### Accountability

You are responsible for the content you add to the blog post. Therefore,
make sure that:

- all the fields are correct and not missing (especially links to
the documentation or feature webpages when available).
- feature availability: add the correct entry (CE, EES, EEP)
- all images are optimized (compressed with [ImageOptim](https://imageoptim.com),
[TinyPNG](https://tinypng.com/), or other tool of your preference) **and** smaller than 300KB.
- if you are adding a gif, it should be compressed as much as possible
and smaller than 300KB. If it's bigger than that, use a video instead.
- all primary features are accompanied by their images.

Write the description of every feature as you do
to regular blog posts. Please write according to
the [markdown guide](/handbook/product/technical-writing/markdown-guide/).

## Monthly release blog post sections
{:#sections}

- Introduction
- CTA buttons
- MVP
- Features
  - Top feature
  - Primary features
  - Secondary features (improvements)
  - Illustrations (screenshots, gifs, or videos)
  accompanying their respective features
  - Up-to-date documentation link
  - Availability
- Performance improvements (added as a secondary feature)
- Omnibus GitLab (added as a secondary feature)
- Upgrade barometer
- Deprecations

### Introduction

Add the introduction to the blog post file (`AAAA-MM-DD-gitlab-X-Y-released.html.md`),
in regular markdown.

Add a short paragraph before the `<!-- more -->` separator, and
conclude the intro below it.


```md
Introductory paragraph (regular markdown)

<!-- more -->

Introduction (regular markdown)
```

### CTA

Call-to-action buttons displayed at the end of the introduction.
A CTA to the [events page](/events/) is added by default. Add webcasts,
or custom buttons to this field whenever necessary.

```yaml
cta:
  - title: Join us for an upcoming event
    link: '/events/'
  - title: Lorem ipsum dolor sit amet
  - link:
```


### MVP

To display the MVP of the month, use the information
provided in this template, and adjust it to your case.
Don't forget to link to the MR with the MPV's contribution.

```yaml
mvp:
  fullname: Dosuken Shinya # full name
  gitlab: dosuken123 # gitlab.com username
  description: | # supports markdown. Pls link to the MR.
    Dosuken extended our [Pipelines API](http://docs.gitlab.com/ce/api/pipelines.html#list-project-pipelines)
    by [adding additional search attributes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9367).
    This is a huge improvement to our CI API, for example enabling queries to easily return the latest
    pipeline for a specific branch, as well as a host of other possibilities. Dosuken also made a great
    contribution last release, laying the foundation for
    [scheduled pipelines](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10133). Thanks Dosuken!
```

### Features

- Feature blocks
  - `name`: feature name, capitalized
  - `available_in`: availability of that feature in GitLab:
    - For Community Edition, use `[ce, ees, eep]`
    - For Enterprise Edition Starter, use `[ees, eep]`
    - For Enterprise Edition Premium, use `[eep]`
  - `documentation_link`: use to insert a link to the documentation. Required for the top feature, optional for primary and secondary features.
  - `documentation_text`: use to customize the text for `documentation_link`. Required for the top feature and optional for primary and secondary features.
  - `image_url`: link to the image which illustrates that feature. Required for primary features, optional for secondary features, n/a for top feature.
  - `image_noshadow: true`: if an image (`image_url`) already has shadow the entry `image_noshadow` will remove the shadow applied with CSS by default. Optional.
  - `description: |`: add the feature's description in this entry. Make sure your cursor is in the line below the pipeline symbol `|` intended once. All `description` fields fully support
  [markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/), the only thing you need to be worried about is respecting the indentation.

<!--

### Top Feature

The top feature is the most important of the release. Fill in the information according to the following example:

```yaml
top:
  - name: Amazing Feature # the feature's name (short title) - required
    available_in: [ees, eep] # (availability of that feature) - required
    documentation_link: 'https://docs.gitlab.com/#amazing' # link to the feature's webpage or documentation - required
    documentation_text: "Learn more about Amazing Feature" # optional
    description: | # supports markdown - required
      Lorem ipsum dolor sit amet, [consectetur adipisicing](#link) elit.
```

### Primary Features

Start with the top feature wrapped into
[one column](#one-column) section.

```html
primary:
  - name: Lorem ipsum # the feature's name (short title) - required
    available_in: [ce, ees, eep] # (availability of that feature) - required
    documentation_link: '/features/cycle-analytics' # link to the feature's webpage or documentation - required
    documentation_text: "Learn more about Cycle Analytics" # optional
    image_url: '/images/9_2/i18n.png' # required
    description: | # supports markdown - required
      Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
      Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis.
```


### Secondary features

Start this part of the blog post with the [loose heading](#loose-headings):

```yaml
secondary:
  - name: Lorem ipsum
    available_in: [eep]
    documentation_link: 'https://docs.gitlab.com/#'
    documentation_text: "Learn more on how to lorem ipsum."
    image_url: '/images/9_2/create_new_merge_request.png'
    description: | # supports markdown - required
      Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
      Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.
```

To display **secondary features**, use the same logic as described
previously for [two columns](#text-on-the-left-image-on-the-right).
The difference is that images will be placed among the content,
not on the right or left of the text. To do so, use two columns
in a row and add all the features in between:

The tricky part is to balance the content between the
columns to make them finish as near to a baseline as possible.

![secondary features - baseline](other-features.png){:.shadow}

  -->

### Cover image license

```yaml
cover_img:
  image_url: '#link_to_original_image'
  licence: CC0 # which licence the image is available with
  licence_url: '#link_to_licence'
```

### Deprecations

```yaml
deprecations:
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
```

For multiple deprecations, use multiple feature blocks:


```yaml
deprecations:
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
```


### Upgrade barometer

```yaml
barometer:
  description: |
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Dignissimos blanditiis reprehenderit voluptate ea quidem
    eveniet similique tempore [quibusdam fugiat](#link) magni, eius,
    quasi aperiam corrupti `tempora` rerum amet totam maiores.
    Reiciendis.
```

### Review

#### Content review

The content review is performed by product managers (PMs),
who will check if everything is in place, and if there's
nothing missing.

Technical writers/editors will follow with
copyedit and check for grammar, spelling, and typos.
Please follow the checklist in the MR description to
guide you through the review.

The review should be finished by the 4th working day before the 22nd.

#### Structural Check

Once the post is filled with content, the technical writing,
UX, or frontend team, will check the syntax and the content structure:

- Filenames
- Frontmatter
  - Authorship
  - Cover image
  - Social sharing image
  - Title
  - Description
  - Category
  - Layout
-  `<!-- more -->` separator (blog intro)
- Deadlinks (Review Apps link)
- Social Sharing card (when published): validate with Twitter Card Validator and Facebook Debugger

##### Frontmatter
{:.gitlab-purple}

Look for each
entry in the frontmatter. Wrap text with double quotes and paths with
single quotes to prevent the page to break due to special chars.

```yaml
---
release_number: "X.X"
title: "GitLab X.X Released with Feature A and Feature B"
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: release
image_title: '/images/X_X/X_X-cover-image.ext'
description: "GitLab 9.0 Released with XXX, YYY, ZZZ, KKK, and much more!"
twitter_image: '/images/tweets/gitlab-X-X-released.jpg'
layout: release
---
```

## Adding content

### Markdown

In fields that support markdown, use regular
[markdown Kramdown](/handbook/product/technical-writing/markdown-guide/),
as we use to all blog posts and webpages for about.GitLab.com.

### Illustrations

#### Images

- Make sure every image has an
[alternative text](/handbook/product/technical-writing/markdown-guide/#image-alt-text)
- Each image should be compressed with [ImageOptim](https://imageoptim.com),
[TinyPNG](https://tinypng.com/), or similar tool
- Each image should not surpass 300KB, gifs included
- If a gif isn't necessary, replace it with a static image (.png, .jpg)
- If an animation is necessary but the gif > 300KB, use a video instead

#### Videos

Every [video should wrapped into a figure tag](/handbook/product/technical-writing/markdown-guide/#videos), as in:

```html
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/PoBaY_rqeKA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
```

The `<figure>` element is recommended for semantic SEO
and the `video_container` class will assure the video
is displayed responsively.

Consult the [markdown guide](/handbook/product/technical-writing/markdown-guide/#videos)
for the correct markdown markup to apply to different sources (YouTube, Google Drive, HTML video).

<!-- 
#### Feature with no images

When a primary feature does not contain a related image,
you can either display it in a section with [one column](#one-column) or
move it to a secondary feature section.

#### Feature with multiple images

When there's more than one image for a single feature, put them
together in their designated column:

![multiple images for a feature](main-multi-img.png){:.shadow}

Do not indent divs, it will not render correctly if you do so.
{:.alert alert-info}

#### Cover image

If the post doesn't have a cover image yet, please
add one, and don't forget to add a [reference](../#cover-image)
to it [at the very end](#cover-image-reference) of the post. -->

<!-- #### Social sharing image

It's recommended to add a [social sharing image](/handbook/marketing/social-marketing/#defining-social-media-sharing-information)
to the blog post. It's the image that will display on
social media feeds whenever the link to the post is shared.
The image should be placed under `source/images/tweets/`
and named after the post's filename (`gitlab-X-X-released.png`).

#### Badges

Every feature should be accompanied by a badge (CE, EES, EEP).
To display them, use the content of the template `available in: (CE/EES/EEP)`,
adding the acronyms to each heading. They can be applied
to `h2` and `h3` elements.

- `## Feature XXX ce ee` will display the badges
`CE` and `EE` (use it for every feature available in GitLab CE)
- `### Feature YYY ees` will display the badge
`EES` (use it for features available in EES and EEP)
- `## Feature ZZZ eep` will display the badge
`EEP` (use it for features available in EEP only) -->


<!-- ## Technical aspects

Understand how a release post is formed:

 -->

<style>
  pre { margin-bottom: 20px; }
</style>
