---
layout: markdown_page
title: "Hiring"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Hiring Introduction

GitLab is committed to hiring the best talent through an open and fair hiring process. Our hiring is managed by our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/) as part of People Operations. The following information on our process has two main objectives:
1. It is meant to be instructive for GitLab employees on how to conduct interviews and best hiring practices.
1. It acts as an informative tool for applicants during the process to better anticipate and prepare for interviews and next steps. For job-specific hiring information, applicants can now reference the *hiring process* section of each job description. For general information review the [typical process](#typical-process) and for details on our interviews refer to [screening](#screening-call), [interviewing](#interviewing), and [interview questions](#ceo-interview-questions).

## Our Hiring Process:

  - [Interviewing Process](/handbook/hiring/interviewing/)
  - [Vacancy Process](/handbook/hiring/vacancy-process/)

## Equal Employment Opportunity

 Diversity is one of GitLab's core [values](https://about.gitlab.com/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and applicants for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](https://about.gitlab.com/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their job duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Ops](/handbook/people-operations/#reach-peopleops).
