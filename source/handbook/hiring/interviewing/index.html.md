---
layout: markdown_page
title: "Interviewing Process"
---

## How to Apply for a Position

The best way to apply for a position with GitLab is directly through our [jobs page](/jobs), where all open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

To apply for a current vacancy:

1. Go to our [jobs page](/jobs).
1. Mid-way down the page, there is a section called "Available Openings".
1. Click on the position title that interests you!
1. This will take you to the job description.
1. If this position interests you, click the "Apply" button at the bottom of page.
1. You will be redirected to a new page hosted by Workable that asks you to fill out basic personal information, provide your resume and cover letter, and answer any application questions.
1. Once you have finished, click "Submit your application" at the bottom.

## Typical Hiring Process

An applicant should expect to wait 2-3 business days between each step of the process. An applicant, at any time, is welcome to contact the recruiter they are working with for an update on their candidacy.

1. **Confirmation of the application**: applicants automatically receive confirmation of their application, thanking them for submitting their information. This is an automated message from Workable. If the person came through another channel they must be added to Workable before continuing the process. There are various ways to do this, see [Workable's documentation](https://resources.workable.com/adding-candidates).
1. Recruiting team does a **first round of evaluations**. Disqualified candidates should be sent a note informing them of the [rejection](#rejecting-applicants). There are templates in Workable to assist, but messages can be tailored as appropriate: place yourself on the receiving end of the message. If more information is required to make the determination, feel free to specifically ask for it (e.g. a cover letter). (If you don't see the templates, you probably haven't [linked your email account to Workable](https://resources.workable.com/support/connecting-gmail-with-workable) yet.)
1. **Pre-screening Questionnaire** Applicants will be sent a pre-screening questionnaire relating to the position by either the recruiting team or the manager to complete and return to the sender. Responses will be added to the candidate's Workable profile.
1. [**Screening call**](#screening-call): in Workable,
   1. Our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/) will do a screening call;
   1. move the applicant to the "screening call" stage in Workable.
   1. send the applicant the Calendly link for the Recruiters, there is a template in Workable.
Depending on the outcome of the screening call, the manager can either [reject an applicant](#rejecting-applicants), or move the applicant to the interview stage in Workable.
1. **Technical interview (optional)**: As described on the [Jobs](/jobs/) page, certain positions require [technical interviews](/jobs/#technical-interview).
1. **Further interviews** that would typically follow the reporting lines up to the CEO. So for instance, the technical interview would be by a co-worker, next interviews would be with the manager / team lead, executive team member, and then the CEO. See below for [sample questions](#interview-questions). The candidate should be interviewed by at least one female GitLab team member.
1. **Reference calls**: Make [reference calls](#reference-calls) for promising candidates. This process can start at an earlier stage, but should happen before an offer is made. At minimum two reference calls should be completed - one to a manager, the other to a colleague. Move the candidate to the "Reference Call" stage in Workable, and ping the relevant person from People Ops to get it going.
1. After reference calls are completed successfully the employment team submits the [employee approval package](#approval-package) to the CEO and Hiring Manager.
1. **CEO interview**: the CEO, as the primary culture carrier of the company, may choose to do an introductory call with a candidate or interview any final round candidates.
Before the CEO has the final call with the applicant the manager will write the following info in an internal note in our ATS:
    * who will extend the offer (CEO is fine with doing this since it is the most speedy for the applicant, but up to the manager)
    * PeopleOps will post the salary information _including compensation calculator link_ in Workable
    * the proposed offer (in all cases, so also when someone else than the CEO makes the offer)
    * in case there are steps before making the final offer (more applicants in the running for the position) this should be noted <br>
1. When the CEO or manager makes the **offer**, this can be done verbal during the call with the applicant, but is always followed quickly with the written offer as described in [the section on preparing offers and contracts](#prep-contracts).
1. People Ops will draft a contract based upon the written offer that was extended.
1. Manager follows up to ensure that the offer is accepted, and that the contract is signed.
1. People Ops [starts the onboarding process](#move-to-onboarding).
1. Manager considers [closing the vacancy](#closing-a-vacancy).

## Screening Call

We conduct screening calls for all positions. This call will be completed by our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/).

Questions are:

1. Why are they looking for a new position?
1. Why did they apply with GitLab?
1. What is your experience with X? (do for each of the skills asked in the position description)
1. Current address? (relevant in context of comp, and in case of contract we will need that information)
1. Full legal name? (relevant in case an offer would be made)
1. How do they feel about working remotely and do they have experience with it?
1. Compensation expectation.
1. Notice period needed
1. STAR questions and simple technical questions may also be asked during the screening call if applicable.

[An example of the output of a good screening call](https://gitlab.workable.com/backend/jobs/128446/browser/applied/candidate/7604850) (need workable account).

At the end of the screening call applicant should be told what the timeline is for what the next steps are (if any).
An example message would be "We are still reviewing applications, but our goal is to let you know in 3 business days from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## CEO Interview Questions

The questions are available in a [Google form](https://docs.google.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) which can be used to save time during the actual interview.
All candidates are asked to fill out the form when they are scheduled for an interview with our CEO to discuss during their call with the CEO.

## Reference calls

As part of our hiring process we will ask applicants to provide us with two or more
references to contact. These reference calls are completed by our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/) following [these guidelines](http://www.bothsidesofthetable.com/2014/04/06/how-to-make-better-reference-calls/).

## Visas & Work Permits

GitLab does not offer full sponsorship for obtaining a H1B visa at this time.
If you already have an H1B visa and were hired, based on proven performance in the role, we would review and consider providing support for transferring your existing H1B. For questions on how to go about transferring an H1B Visa please see [People Operations](https://about.gitlab.com/handbook/people-operations/visas/#h1b-visa-processing).

For work permits and visas in The Netherlands please refer to [Dutch Work Permits](https://about.gitlab.com/handbook/people-operations/#dutch-work-permits) in our handbook.
