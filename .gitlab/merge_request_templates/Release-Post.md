Release Post :tada:

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/

### General Contributions

General contributions from the team.

Due date: MM/DD (6th working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release 
- [ ] Add milestone 
- [ ] Intro
- [ ] MVP
- [ ] Add cover image `image_title` (compressed)
- [ ] Upgrade barometer
- [ ] Top features
- [ ] Primary features
- [ ] Secondary features (improvements)
- [ ] Performance improvements
- [ ] Omnibus improvements
- [ ] Deprecations
- [ ] Documentation links
- [ ] Authorship (author's data)

### Review

#### Content review (PMs)

Due date: MM/DD (2nd working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] General review (PM)
  - [ ] Check Features' names
  - [ ] Check Features' availability (CE, EES, EEP)
  - [ ] Check Documentation links
  - [ ] Update `data/promo.yml`
  - [ ] Check all images' size - compress whenever > 300KB
  - [ ] Meaningful links (SEO)
  - [ ] Check links to product/feature webpages
- [ ] Copyedit (Sean P, Rebecca, or Marcia)
  - [ ] Title
  - [ ] Description
  - [ ] Grammar, spelling, clearness (body)
- [ ] Final review (Job)

#### Structural Check

Due date: MM/DD (2nd working day before the 22nd)

- 1. Structural check
- [ ] Label MR: ~"blog post" ~release ~review-structure
- [ ] Check frontmatter (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow `{:.shadow}`
- [ ] Check images' `ALT` text
- [ ] Videos/iframes wrapped in `<figure>` tags (responsiveness)
- [ ] Add/check `<!-- more -->` separator
- [ ] Add/check cover img reference (at the end of the post)
- [ ] Columns (content balance between the columns)
- [ ] Badges consistency (applied to all headings)
- [ ] Remove any remaining instructions
- [ ] Remove HTML comments
- [ ] Run deadlink checker
- [ ] Update release template with any changes (if necessary)
